from flask_login import current_user
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms.fields.simple import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length, ValidationError, EqualTo
from webAppProject.models import User, CreateEmailMask

#Handling the form to create a new user
class RegistrationForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])

    username = StringField('Username', validators=[DataRequired(),
                        Length(min=2, max=20)])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                    validators=[DataRequired(),
                                                EqualTo('password')])
    submit = SubmitField('Sign Up Today')

    
    # Functions to enable validation if the user already exists within the database and throws an error if so
    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError("That username is already taken, please choose another one")
    
    def validate_email(self, email):
        user_email = User.query.filter_by(email=email.data).first()
        if user_email:
            raise ValidationError("That email is already taken please choose another one")

# Handling the form for when the user wants to login 
class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log in')

#Handling the form for when the user wants to create an email mask
class EmailMask(FlaskForm):
    mask = StringField("Email Mask", validators=[DataRequired()])
    submit = SubmitField("Create")

    # Function to put validation if the data entered contains an "@" and throws an error if so
    def validate_mask(self, mask):
        if '@' in mask.data:
            raise ValidationError("The @ will be added on automatically, please do not add it on.")
# Handles the form for when the user wants to update their account information
class UpdateAccountForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired(),
                        Length(min=2, max=20)])
    profile_picture = FileField("Update Profile Picture", validators=[FileAllowed(["jpg", "jpeg", "png"])])
    submit = SubmitField("Update")
    # Functions for seeing if the username/email already exists and throws an error if so
    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError("That username is already taken, please choose another one")
    
    def validate_email(self, email):
        if email.data != current_user.email:
            user_email = User.query.filter_by(email=email.data).first()
            if user_email:
                raise ValidationError("That email is already taken please choose another one")