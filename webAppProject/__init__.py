from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager # Manages the user's logins
db = SQLAlchemy() # - Handles creating the instance of our database
database_name = "database_privacy.db"
def create_app():
  app = Flask(__name__)
  app.config['SECRET_KEY'] = 'b7677dfd44524965a0464233c0941bf5'
  app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{database_name}"
  db.init_app(app)

  from webAppProject.models import User, CreateEmailMask

  with app.app_context(): # - Handles the creation of our database
      create_database(app)
  login_manager = LoginManager()
  login_manager.login_view = "login"
  login_manager.init_app(app)
  @login_manager.user_loader
  def load_user(id):
    return User.query.get(int(id))
  return app 
 

def create_database(app):
    """
    Checks to see if the database has been created yet and if not creates the database
    """
    if not path.exists("webAppProject/" + database_name):
        db.create_all()
        print("Database created")