from flask_login import UserMixin
from flask import current_app
from datetime import datetime
from webAppProject import db

#The class that handles the information that will releate to every user that signs up and uses the app
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(150), unique=True, nullable=False)
    username = db.Column(db.String(50), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default="default.jpg")
    password = db.Column(db.String(60), nullable=False)
    masks = db.relationship('CreateEmailMask') # Used for a one-to-many relationship to have many email masks to one user

#The class that will handle the related fields that a
class CreateEmailMask(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    content = db.Column(db.String(50), nullable= False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


