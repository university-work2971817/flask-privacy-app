# Main Objectives in Building Flask Privacy Web App

Here is the plan that I will be following to create parts of my Privacy App from my Computional Thinking App Design.  
 I have added the Account page from the initial specifications as it was an oversight not to have one in.  
I have used tutorials from Corey Schafer and Tech With Tim as learning material in the learning of Flask using ways they did, deleting code and seeing how it worked while applying it to the task I am doing.  
I have decided not to use Blueprints functionality of flask as I am not building a site large enough that it would be needed.  
Corey Schafer Learning Flask Playlist - <https://www.youtube.com/playlist?list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH>  
Tech With Tim Building a Flask Web App -  <https://www.youtube.com/watch?v=dam0GPOAvVI>

- Create a Home Page
- Create a Log In Page
- Create a Register Page
- Create An Email Mask Page
- Create An Account Page
- Create a Database with SQLAlchemy

## All Pages

- A navbar at the top directing you to the other pages
- If you are not logged in, it will only show the signup and login buttons and if you are logged in, it will not show the sign up and login elements but replace them with sign out
- To the right of the navabar will be an image of your account with the name field to the right of it.

## Register Page

- If the register is done correctly REDIRECTS the user back to the Log In Page
- Have a username field which is the User's email
  - Validation on this part checking if it is a right email or if it already exists on the signed up users in the database. If it does, throws out an error message saying it is already in use or doesn't exist.
- Have a name field which is the user's names
  - Have validation so two user's can have the same name, throws an error message saying the name is already in use
- Have a password input field
  - Make sure they can't see what they are typing in
- Have a confirm password input field
  - Same as the password field but have validation that is checking to see if it is equalling the same data as what is in the password field and if it is not, throws out an error.
- In the code, they will be also a default user image set for the user which they can change after making it

## Log In Page

- It wave a username (email field) and password field. They will be a Register button and a log in button
- If you get either the username or password wrong it will throw out an error to you that is not a valid username or password while not saying which for security reasons.

## Home Page

- The Home element button on the navbar will be underlined and highlighted to signify you are on this page
- It will contain general information about the app

## Email Mask

- The email mask button on the navbar will be underlined and highlighted
- All the email masks will be displayed in a list which is looped through and displaying one after the other
- There will be a button to go to its own page to create an email mask

## Create Email Mask

- It will contain the app name to the left, and the name of the current user to the right
- It will then contain a heading called Create Email Mask that is aligned to the centre
- In the middle of the page there will be an input box to enter the name that you will use for the email mask that will have validation to make sure they don't add an `@example.com` or anything like that after it as that is added automatically.
- They will be towards the middle of the bottom of the page that will create the Email Mask which will add it to the database (a directory - list will function for sample data before the database is created). The create button will be what adds it to the database/append it to the list then redirect to the email mask page. The back button will put the user back on the email mask page as well.

## Account

- Below the navbar it will have the current profile picture of the user alongside to right it will have their followed by their email address below it
- Below that is their account info form that automatically enters their current username and email and below that there is a field that allows for browsing their computer for a new profile picture and finally a button to update it
- On these forms fields they will validating that stops the user from updating their values of the username and email to ones that already exists . The profile picture will have validation on formats that are not jpg or png.
