import secrets, os
from flask import Flask, render_template, request, redirect, flash, url_for
from flask_login import login_user, logout_user, current_user, login_required
from flask_sqlalchemy import SQLAlchemy
from webAppProject.forms import RegistrationForm, LoginForm, EmailMask, UpdateAccountForm
from webAppProject import create_app, db
from werkzeug.security import generate_password_hash, check_password_hash #Secures the passwords the users will be entering
from webAppProject.models import User, CreateEmailMask

app = create_app()

#Handles the routing to the Home Page
@app.route("/")
@app.route("/home")
def home():  
    return render_template("home.html", title="Home")

#Routing towards the email mask page
@app.route("/email_mask", methods=['GET', 'POST'])
def email_mask():
    return render_template("email_mask.html", title="Email Mask", user=current_user)

#Routes and requests to enable the user to create an email mask
@app.route("/create_mask", methods=["GET", "POST"])
def create_mask():
    """
    This function while functioning as a route also allows the user to create a new email mask
    If all the validation within the form is create it will create the mask with the email domain on it
    for the current user and then add + commit it to the database and send out a message saying it was created
    redirecting them to the main email mask page
    """
    form = EmailMask()
    if form.validate_on_submit():
        create_new = CreateEmailMask(content=form.mask.data + "@emailexample.com", user_id=current_user.id)
        db.session.add(create_new)
        db.session.commit()
        flash("Mask Added", "success")
        return redirect(url_for('email_mask'))
    return render_template("create_mask.html", title="Create Email Mask", form=form, user=current_user)

#Handles the routing for the login page
@app.route("/login", methods=['GET', 'POST'])
def login():
    """
    Function that lets the user log in.  It will check if the data entered within the form equals a user in the database and check it against the hash password
    and if they all match it will say they have logged in and redirect them to the home page.
    """
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                flash("You have logged in!", "success")
                return redirect (url_for('home'))
    return render_template("login.html", title="Login", form=form)



#Handles saving the new picture when a user wants to change their profile picture.
def save_picture(form_picture):
    """
    This function purpose is that it will get a random hex value for the filename and add whatever file extension the picture the user uploads and returns that.
    This will allow for changing the user's profile picture from the default image.
    """
    random_hex = secrets.token_hex(8)
    file_name, file_ext = os.path.splitext(form_picture.filename)
    picture_filename = random_hex + file_ext
    picture_path = os.path.join(app.root_path, "static/profile_pics", picture_filename)
    form_picture.save(picture_path)
    return picture_filename


# Route to the register page
@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data)
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(message='Your account has been created! You are now able to log in', category= 'success')
        return redirect(url_for('login'))
    return render_template("register.html", title='Register', form=form)

# Route to the account page
@app.route("/account", methods=["GET", "POST"])
def account():
    """
    This function enables the user to change their account email, their account picture and their account username.
    """
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.profile_picture.data:
            picture_file = save_picture(form.profile_picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash("Account has been updated", "success")
        return redirect(url_for("account"))
    image_file = url_for("static", filename="profile_pics/" + current_user.image_file)
    return render_template("account.html", form = form, user = current_user, image_file = image_file)

# Route enabling the user to logout from their account
@app.route("/logout")
@login_required
def logout():
    """
    This function simply logs out the user
    """
    logout_user()
    return redirect(url_for("home"))

if __name__ == '__main__':
    app.run(debug=True)
